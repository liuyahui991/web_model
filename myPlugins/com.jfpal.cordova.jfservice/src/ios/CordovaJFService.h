//
//  CordovaJFService.h
//  CordovaJFService
//
//  Created by Wangjing on 15/4/13.
//  Copyright (c) 2015年 jfpal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>

@interface CordovaJFService : CDVPlugin

/*! 获取运行时信息
 \param  command cordova命令对象
 \return 无
 */
- (void)getEnv:(CDVInvokedUrlCommand*)command;


/*! RSA加密
 \param  command cordova命令对象
 \return 无
 */
- (void)RSAEncode:(CDVInvokedUrlCommand *)command;


/*! md5 加密
 \param  command cordova命令对象
 \return 无
 */
- (void)MD5Encode:(CDVInvokedUrlCommand *)command;


/*! 获取本地存储信息
 \param  command cordova命令对象
 \return 无
 */
- (void)getPerference:(CDVInvokedUrlCommand *)command;


/*! 设置本地存储信息
 \param  command cordova命令对象
 \return 无
 */
- (void)setPerference:(CDVInvokedUrlCommand *)command;


@end


