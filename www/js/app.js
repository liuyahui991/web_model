angular.module('starter', ['ionic','ngCordova','ng.lodash', 'controllers', 'services','ngCordova.plugins.jfservice', 'easypiechart','toaster','flow','chart.js','angular-loading-bar', 'ngAnimate','AppAnimations'])
.run(function($ionicPlatform,$location,$ionicHistory) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.backgroundColorByName('white');
    }
  });
  $ionicPlatform.registerBackButtonAction(function (e) {
    if ($location.path() == '/app' ) {
      $window.context.quit();
    }else if ($ionicHistory.backView()){
      $ionicHistory.goBack();
    }
  }, 100);
  // $http.defaults.headers.common["Content-Type"] = 'application/x-www-form-urlencoded'
})
.config(function($ionicConfigProvider){
  $ionicConfigProvider.backButton.text("&nbsp;&nbsp;").previousTitleText(false);
})
.run(function($rootScope,$state ,toaster){
  // toasterRegisterEvents.registerClearAllToastsEvent();
  $rootScope.$on("network:error", function(){
        msg = "网络错误，请退出重试";
        return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
  });
})
.constant("API_URL", "http://192.168.50.89:2016/")//140.207.48.54:18012
.config(['flowFactoryProvider', function (flowFactoryProvider) {
    flowFactoryProvider.defaults = {
        target: 'http://192.168.50.89:2016/upload',
        permanentErrors:[404, 500, 501]
    };
    // You can also set default events:
    flowFactoryProvider.on('catchAll', function (event) {
     console.log('cathAll');
    });
    // Can be used with different implementations of Flow.js
    // flowFactoryProvider.factory = fustyFlowFactory;
}])
.config(function($ionicConfigProvider) {
  // $ionicConfigProvider.views.maxCache(5);

  // note that you can also chain configs
  // $ionicConfigProvider.backButton.text('Go Back').icon('ion-chevron-left');
  $ionicConfigProvider.views.transition('ios');
  $ionicConfigProvider.scrolling.jsScrolling('true');
  $ionicConfigProvider.tabs.style('standard');
  $ionicConfigProvider.tabs.position('bottom');
});
;