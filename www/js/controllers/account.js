angular.module('controllers')
.controller('AccountCtrl', function($scope, $r,toaster,$timeout) {
      $scope.form={};
      $scope.form.starttime=true;
       var loginBody= [{}];
        $scope.$on('$ionicView.enter', function() {
             console.log("$ionicView.enter");
             //laber1
             $scope.labels_1 = ["06/15", "07/15", "08/16", "09/15", "10/15", "11/15", "12/15"];
            $scope.data_1 = [
              [65, 59, 80, 81, 56, 55, 40]
            ];
           $scope.colours=[{fillColor:'rgba(236, 140, 11, 0.5)',strokeColor: 'rgba(236, 140, 11, 0.8)',
    }];
            $scope.onClick = function (points, evt) {
              console.log(points, evt);
            };

            // Simulate async data update
            $timeout(function () {
              $scope.data_1 = [
                [28, 48, 40, 19, 86, 27, 90]
              ];
            }, 3000);
            //laber2
          $scope.labels_2 =["06/15", "07/15", "08/16", "09/15", "10/15", "11/15", "12/15"];
          $scope.data_2 = [
            [65, 59, 90, 81, 56, 55, 40],
            [28, 48, 110, 219, 96, 27, 100],[128, 48, 110, 119, 196, 127, 100]
          ];

         //laber4
         $scope.labels_4 = ["Download Sales", "In-Store Sales", "Mail-Order Sales","Nsij"];
          $scope.data_4= [300, 500, 100, 200];
            });

})
.controller('WithdrawalCtrl', function($scope, $state, toaster, validation ,$r ,$cache ,$ionicModal ,$filter) {
      $scope.form={};
      $scope.form.money;
      $scope.form.countdown="发送验证码";
      $scope.form.sendmsgs=true;
      $scope.form.submits=true;
      $ionicModal.fromTemplateUrl('templates/sendmsg.html', {
        scope: $scope
      }).then(function(modal) {
        $scope.modal = modal;
      });
      //rules
   $ionicModal.fromTemplateUrl('templates/showrules.html', {
          scope: $scope
        }).then(function(modal) {
          $scope.showrules = modal;
      });
      $scope.form.clickstatus='tab1';

         var loginBody= [{}];
          $r("myAccount", loginBody ).then(function(data){
                  console.log(data);
                   if(data.respHead.code=="0000"){
                        $scope.form.account=data.respBean.data[0];
                      }else{
                        var msg=data.respHead.message;   
                        return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                      }
                  
          },function(err){
                  console.log(err);
          });       
                  
      $scope.$on('$ionicView.enter', function() {
             console.log("$ionicView.enter");
                   var loginBody= [{}];
                  $r("myAccount", loginBody ).then(function(data){
                          console.log(data);
                           if(data.respHead.code=="0000"){
                                $scope.form.account=data.respBean.data[0];
                              }else{
                                var msg=data.respHead.message;   
                                return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                              }
                          
                  },function(err){
                          console.log(err);
                  }); 

                  if($cache.getBank('withdraw')){
                  $scope.form.bank=$cache.getBank('withdraw');
                  console.log();
                   }else{
                 var product_bank=[{}];
                $r("bankCardList", product_bank ).then(function(data){
                           console.log(data);
                            if(data.respHead.code=="0000"){
                              $scope.form.bank=data.respBean.data[0];
                            }else{
                              var msg=data.respHead.message;   
                              return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                            }
                    
                },function(err){
                    console.log(err);
                }); 

              }
        });

    //rules
      $scope.showrulesview=function(){
         $scope.showrules.show();
      }
      $scope.closerules = function() {
          $scope.showrules.hide();
        };
      $scope.tabshow=function(status){
          $scope.form.clickstatus=status;
      }

      $scope.checkclick=function(){
         var msg;
         console.log(validation.money($scope.form.money));
         console.log($scope.form.money<=$scope.form.account.balance);
          console.log($scope.form);
          var moneys=parseFloat($scope.form.money)*100;
        if(validation.isnull($scope.form.money)){
              if(validation.money(moneys)){
                if(moneys>=1){
                    if(moneys<=$scope.form.account.balance) //$scope.form.money<=$scope.form.account.balance
                    {
                      console.log($scope.form.bank);
                      if($scope.form.bank){
                        $scope.modal.show();
                      }else{
                        msg = "请选择一张银行卡";
                      }
                    }else{
                        msg = "提现金额不能超过账户余额";
                    }
                }else{
                   msg = "请输入合法金额";
                }
              }else{
                  msg = "请填写正确的提现金额";
              }
        }else{
               msg = "提现的金额不能为空";
        }

        if(msg){
          return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
        }
      
    };
   $scope.closemodal = function() {
        $scope.modal.hide();
    };
  $scope.sendmsg=function(){

    if($scope.form.sendmsgs==true){
        $scope.form.sendmsgs=false;
        var timer=40;
        var myTime = setInterval(function() 
          { 
            timer--;
            $scope.form.countdown='等待'+timer+ 's';
            $scope.$digest(); // 通知视图模型的变化
          }, 1000); // 倒计时10-0秒，但算上0的话就是11s ]

         setTimeout(function() { 
          clearInterval(myTime);
           $scope.form.countdown="重新发送"; 
           $scope.form.sendmsgs=true;
           $scope.$digest(); 
        }, 41000);

         $scope.form.bizType='1001';
        var getValidateNo=[{
                      'bizType': $scope.form.bizType,
                     }];
        $r("getValidateNo", getValidateNo ).then(function(data){
           console.log(data);
            if(data.respHead.code=="0000"){
                //$scope.form.msg=data.respBean;
              }else{
                var msg=data.respHead.message;   
                return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
              }
          },function(err){
              console.log(err);
          }); 
        
    }else{
                var msg="验证码已发送";   
                return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
    }
    
   }
  $scope.checkmsg=function(obg){
    console.log(obg);
     var msg;
      if(validation.isnull(obg.msg))
      {
          if(validation.isnull(obg.password))
          {
                $scope.form.submits=false;
              
              //todo withdraw
               var withdrawDeposit=[{
                'amount':parseFloat(obg.money)*100,
                'cardNo':obg.bank.cardNo,
                'checkCode':obg.msg,
                'bizType':'1001',
                "password":obg.password,
               }];
               console.log(withdrawDeposit);
                  $r("withdrawDeposit", withdrawDeposit ).then(function(data){
                     console.log(data);
                     if(data.respHead.code=="0000")
                     {
                      $scope.modal.hide();
                      msg='提现成功';   
                      $scope.$emit("$ionicView.enter");
                     }else{
                       msg=data.respHead.message;   
                    }
                    $scope.form.submits=true;
                    return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                  },function(err){
                       $scope.form.submits=true;
                      console.log(err);
                      var msg=err.respHead.message;   
                            return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                  }); 
            //$scope.modal.hide();
          }else{
             msg = "登录密码为空";
             return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
          }
      }else{
        msg = "验证码为空";
        return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
      }
  }



})
.controller('EarningsCtrl', function($scope, $r, toaster) {
   $scope.form={};
  var reqBody= [{}];
  $r("incomeList", reqBody ).then(function(data){
                  console.log(data);
                    if(data.respHead.code=="0000"){
                        $scope.form.incomeList=data.respBean;
                    }else{
                      var msg=data.respHead.message;   
                      return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                    }
                   
                },function(err){
                  console.log(err);
              });
})
.controller('HadboughtCtrl', function($scope, $r, $ionicPopup, toaster, validation,$filter) {
        $scope.form={};
        $scope.form.boughtlist;
        $scope.form.transfer;
            var msg;
            var load = function() {
            var reqBody= [{}];
              $r("amountList", reqBody ).then(function(data){
                  console.log(data);
                    if(data.respHead.code=="0000"){
                        $scope.form.boughtlist=data.respBean;
                    }else{
                      var msg=data.respHead.message;   
                      return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                    }
                   
                },function(err){
                  console.log(err);
              });
            }
        $scope.$on('$ionicView.enter', function() {
             console.log("$ionicView.enter");
             load();
        });
        $scope.transferOut=function(item){
          console.log(item);
          $ionicPopup.show({
              template: '<div class="input-price"><input type="tel" class="bargain-input" ng-model="form.transfer" ><span>元</span></div>',
              title: '赎回',
              subTitle: '<span>您可以赎回的金额应小于等于本金＋收益</span>',
              scope: $scope,
              buttons: [
                { text: '取消',
                  onTap: function () {
                  }
                },
                {
                  text: '<b>确定</b>',
                  type: 'button-positive',
                  onTap: function(e) {
                    var transfermoney=parseFloat($scope.form.transfer)*100
                          if(validation.isnull($scope.form.transfer))
                          { 
                              if(validation.money( transfermoney)){
                                if(transfermoney>=1){
                                     if ( transfermoney>(item.investAmount+item.yield))  {
                                    //don't allow the user to close unless he enters wifi password
                                        toaster.pop('warning', null, '<ul><li> 赎回的金额必须小于等于' + $filter('tomoney')(parseFloat(item.investAmount+item.yield))  + '元</li></ul>', null, 'trustedHtml');
                                        e.preventDefault();
                                      } else {
                                        var reqBody= [{
                                          'productCode':item.productCode,
                                          'amount':transfermoney,
                                        }];
                                      $r("transferOut", reqBody ).then(function(data){
                                              console.log(data);
                                               if(data.respHead.code=="0000"){
                                                    $scope.$emit("$ionicView.enter");
                                                    var msg=data.respHead.message;   
                                                    return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                                                }else{
                                                  var msg=data.respHead.message;   
                                                  return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                                                }
                                            },function(err){
                                              console.log(err);
                                          });
                                      }
                                }else{
                                       msg = "请输入合法金额";
                                       return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
                                    }
                              }else{
                                msg = "请填写正确的赎回金额";
                                return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
                              }
                          }else{
                                 msg = "赎回的金额不能为空";
                                 return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
                          }
                       
                  }
                }
              ]
          });


        } 
})
.controller('TradingrecordCtrl', function($scope, toaster, $r) {
      $scope.clickstatus='01';
      $scope.form={};
      $scope.form.borrowlist;
      $scope.form.borrower_mobile;
      var msg;
      var load = function() {
              var reqBody= [{
                'transType':$scope.clickstatus
              }];
              $r("transList", reqBody ).then(function(data){
                  console.log(data);
                   if(data.respHead.code=="0000"){
                        $scope.form.borrowlist=data.respBean.data;
                        $scope.form.transFlowId=data.respBean.data[0].transFlowId;
                        console.log(data.respBean.data);
                    }else{
                      var msg=data.respHead.message;   
                      return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                    }
                   
                },function(err){
                  console.log(err);
              });
          }
      $scope.$on('$ionicView.enter', function() {
             console.log("$ionicView.enter");
             load();
        });

    $scope.form.hrefstatus = "";
    $scope.click=function(status){
      if (status === '00'){
        $scope.form.hrefstatus = "#/app/transdetail/";
      }
      $scope.clickstatus=status;
      load();
    }
})
// transdetailCtrl
.controller('transdetailCtrl', function($scope,$r,$http,$state,toaster,$stateParams) {
      $scope.form={};
      $scope.$on('$ionicView.enter', function() {
            var transdetail_body=[{
                "transFlowId":$stateParams.transFlowId
                }];
            $r("transDetail", transdetail_body ).then(function(data){
                console.log(data);
                $scope.form=data.respBean.data[0];
                console.log($scope.form);
              },function(err){
                  console.log(err);
             }); 
             console.log("$ionicView.enter");
      });
})
.controller('SendmsgCtrl', function($scope, $state, toaster, validation) {
     $scope.setFormScope = function(scope) {
      $scope.formScope = scope;
      };
    $scope.checkclick=function(){
       var sendmsgFrom = $scope.formScope.sendmsgFrom;
       console.log(sendmsgFrom);
       var msg;
      if(!$scope.formScope.sendmsgFrom.$valid){
            if(sendmsgFrom.code.$error.required){
              msg = "验证码为空";
            }else if(sendmsgFrom.password.$error.required){
              msg = "登录密码为空";
            }else {
              msg = "出错";
            }
            return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
        }else{
           $state.go("app.sendmsg");
        }
  };
})

.controller('GoBack', function($scope,$http,$state,$rootScope,$stateParams,$ionicHistory,$window) {
    var home = $stateParams.home;
  $scope.back = function() {
      if(home){
          $window.context.quit();
      } else {
        $ionicHistory.goBack();
      };
    }
  
})
;