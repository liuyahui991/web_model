angular.module('controllers')
.controller('ArticlelistCtrl', function($rootScope,$scope,$rs,toaster,validation,$cache,_,$ionicModal) {
      $scope.form={};
      $scope.form.pageIndex=1;
      $scope.form.borrowlist;
       $ionicModal.fromTemplateUrl('templates/articledetail.html', {
        scope: $scope
      }).then(function(modal) {
        $scope.detail = modal;
      });
      $ionicModal.fromTemplateUrl('templates/upcircle.html', {
        scope: $scope
      }).then(function(modal) {
        $scope.modal = modal;
      });
      var load = function() {
                var reqBody= {
                  "pageNum":1,
                  "numPerPage":6,
                };
                $rs("ArticleList", reqBody ).then(function(data){
                  console.log(data);
                   if(data.code=="200"){
                        $scope.form.borrowlist=data.respondata;
                        $scope.$broadcast('scroll.refreshComplete');
                        $scope.noMoreItemsAvailable = false;
                        $scope.form.pageIndex=1;
//                        $scope.form.pageIndex=data.respBean.page.pageIndex+1;
//                        $scope.form.totalRecord=data.respBean.page.totalRecord;
//                        $scope.form.surplusBorrwoer=data.respBean.surplusBorrwoer;
                    }else{
                      var msg=data.message;   
                      return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                    }
                },function(err){
                  console.log(err);
              });
          };
       $scope.$on('$ionicView.enter', function() {
             console.log("$ionicView.enter");
             load();
       });
      $scope.doRefresh=function(){
        load();
      };
       
      $scope.noMoreItemsAvailable = false;
      $scope.loadMoreData = function() {
         var reqBody= {
              "pageNum":$scope.form.pageIndex+1,
              "numPerPage":6,
            };
         $rs("ArticleList", reqBody).then(function(data){
//           debugger;
            console.log(data);
             if(data.code=="200"){
//               $scope.noMoreItemsAvailable = true;
                  if($scope.form.borrowlist && $scope.form.borrowlist.length >= data.page.totalRecord){
                      $scope.noMoreItemsAvailable = true;
                  }else{
                    if($scope.form.borrowlist){
                      _.each(data.respondata, function(item){
                      $scope.form.borrowlist.push(item);
                      });
                      $scope.form.pageIndex=data.page.pageIndex+1;  
                    }
                    
                  }
              }else{
                $scope.noMoreItemsAvailable = true;
                var msg=data.message;   
                return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
              }
          },function(err){
            $scope.noMoreItemsAvailable = true;
            console.log(err);
        });
        $scope.$broadcast('scroll.infiniteScrollComplete');
        
      };
      
      //delete artlist
      $scope.del_bower=function(obj) {
           var reqBody= {
                  "id":obj._id
                };
           $rs("DelArticle", reqBody ).then(function(data){
                  console.log(data);
                   if(data.code=="200"){
                        $scope.$emit("$ionicView.enter");
                    }else{
                      var msg=data.message;   
                      return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                    }
                },function(err){
                  console.log(err);
              });
      };
      //show articles
      $scope.showarticle=function(obj){
               $scope.detail.show();
             var reqBody= {
                      "id":obj._id
                    };
               $rs("finActicle", reqBody ).then(function(data){
                      console.log(data);
                       if(data.code=="200"){
                            $scope.form=data.docs[0];
                        }else{
                          var msg=data.message;   
                          return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                        }
                    },function(err){
                      console.log(err);
                  });
      };
      $scope.closedetail = function() {
          $scope.detail.hide();
          load();
      };

      $scope.addcommit=function(obj){
        console.log(obj);
        if(!$cache.getUserName()){
          $scope.detail.hide();
          $rootScope.$broadcast("token:invalidation");
          return;
        }
          var reqBody= {
                  "id":obj._id,
                  "comment":{
                    'user':$cache.getUserName(),
                    'repayuser':obj.user,
                    'content': obj.thecomment,
                    'time':new Date(),
                  }
                };
           $rs("MKcommit", reqBody ).then(function(data){
                  console.log(data);
                   if(data.code=="200"){
                    if($scope.form.comment){
                      $scope.form.comment=$scope.form.comment;
                    }else{
                      $scope.form.comment=[];
                    }
                        $scope.form.comment.push(reqBody.comment);
                    }else{
                      var msg=data.message;   
                      return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                    }
                },function(err){
                  console.log(err);
              });
        
      };
      //弹出update model
      $scope.showupdate=function(obj){
         $scope.modal.show();
         var reqBody= {
                  "id":obj._id
                };
           $rs("finActicle", reqBody ).then(function(data){
                  console.log(data);
                   if(data.code=="200"){
                        $scope.form=data.docs[0];
                    }else{
                      var msg=data.message;   
                      return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                    }
                },function(err){
                  console.log(err);
              });
      };
      $scope.closemodel = function() {
          $scope.modal.hide();
          load();
      };
      //处理update方法
      $scope.updarticle=function(obj){
        var msg;
        if(validation.isnull(obj.tirtle)){
          if(validation.isnull(obj.content)){
            msg=null;
             var reqBody= {
                  "id":obj._id,
                  "tirtle":obj.tirtle,
                  "content":obj.content,
                };
            $rs("updCirtle", reqBody).then(function(data){
              console.log(data);
              if(data.code=='200'){
                msg = "修改文章成功";
                $scope.modal.hide();
                load();
                return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
              }else{
                msg = "修改文章失败";
                return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
              }
            },function(err){
              console.log(err);
            });
          }else{
              msg = "文章内容不能为空";
          }
        }else{
              msg = "文章标题不能为空";
        }
        if(msg){
           return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
        }

      };
})
.controller('ChriteCtrl', function($scope,$rs,toaster,validation,$cache,$ionicHistory) {
  $scope.form={};
  var msg;
  $scope.form.username=$cache.getUserName();
  $scope.pubArticle=function(data){
    console.log(data);
    if(validation.isnull(data.tirtle)){
      if(validation.isnull(data.content)){
        msg=null;
        var reqBody= {
          "tirtle":data.tirtle,
          "users":$cache.getUserName(),
          "content":data.content,
        };
        $rs("cretirtle", reqBody).then(function(data){
          console.log(data);
          if(data.code=='200'){
            msg = "添加文章成功";
            $ionicHistory.goBack();
            return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
          }else{
            msg = "添加文章失败";
            return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
          }
        },function(err){
          console.log(err);
        });
      }else{
          msg = "文章内容不能为空";
      }
    }else{
          msg = "文章标题不能为空";
    }
    if(msg){
       return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
    }
  };
  $scope.cancleCre=function(){
    $ionicHistory.goBack();
  };
})
;