angular.module('controllers')
.controller('makeBankCtrl',function($scope,$r,toaster,validation,$cache,$ionicHistory){
  $scope.form={};
  $scope.form.bank ;

  $scope.form.idcard=$cache.getIdCard();
  $scope.form.username=$cache.getUserName();
  $scope.form.submits=true;
var cache = $scope.cache = {};
//all the bank list

// var bank_list=[{}];
// $r("bankList", bank_list ).then(function(data){
//   $scope.banklist=data.respBean;
//   console.log($scope.banklist);
//    cache.banks = $scope.banklist.data;
//   },function(err){
//   console.log(err);
// }); 

 $scope.select_banks = function(item){
  console.log(item);
    $scope.form.bank = item.bankName;
    // $r("address.county").query({id: $item.id}, function(counties){
    //     address.countries = counties;
    // })
  };

var  addbank = $scope.addbank={};
addbank.cardType="01";
addbank.certNo=$scope.form.idcard;
addbank.userName=$scope.form.username;
console.log(addbank.certNo);
$scope.addmybank=function(){
  var msg;
  if(!validation.isnull(addbank.cardNo)){
        msg = "输入银行卡卡号";
        return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
  }else if(!validation.cardid(addbank.cardNo)){
        msg = "银行卡卡号格式错误";
        return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
  }else if(!validation.isnull(addbank.cardType)){
        msg = "输入卡类型";
        return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
  }else if(!validation.isnull(addbank.mobileNo)){
        msg = "输入手机号";
        return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
  }else if(!validation.phone(addbank.mobileNo)){
        msg = "手机号格式错误";
        return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
  }else{
                  $scope.form.submits=false; //拒绝重复提交
          var bank_add=[
              {
                "cardNo":addbank.cardNo,
               // "bankName":addbank.bankName,
                "userName":addbank.userName,
                "certType":'00',//证件类型
                "certNo":addbank.certNo,
                "opType":'1',
                "cardType":addbank.cardType,
                "mobileNo":addbank.mobileNo,
              }
          ];
          $r("bankCard", bank_add ).then(function(data){
                var msg;
                console.log(msg);
                console.log(data);
                if (data.respHead.code==="0000") {
                  $ionicHistory.goBack();
                  $scope.$emit("$stateChangeSuccess");
                  msg="银行卡绑定成功";
                   $scope.form.submits=true;
                   return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
                }else{
                   $scope.form.submits=true;
                  msg=data.respHead.message;
                    return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
                }
            },function(err){
            console.log(err);
          }); 
    }
}
})

.controller('choseBankCtrl',function($scope, $r, $ionicHistory, $state ,$cache, toaster){
var banklength;
 var bank_chose=[{}];
$scope.$on('$stateChangeSuccess',function(){
     $r("bankCardList", bank_chose ).then(function(data){
        $scope.banklist=data.respBean;
        console.log(data);
          if ($cache.getBank('withdraw')) {
          }else{
            $cache.setBank('withdraw',$scope.banklist.data[0]);
          };
        },function(err){
        console.log(err);
      }); 
 })

 $scope.chosebank=function(item){
  console.log(item);
  $cache.setBank('withdraw',item);
  $ionicHistory.goBack();
 }

 //del bank
     $scope.delbank=function(item){
      console.log(item);
         var del_bank=item;
            var bank_del=[{
                    "cardNo":del_bank.cardNo,
                    "opType":'2',
                    "userBankId":del_bank.userBankId
            }]
         $r("bankCard", bank_del ).then(function(data){
              var msg;
              msg=data.respHead.message;
              if (data.respHead.code==="0000") {
                    $cache.setBank('withdraw','');
                    $scope.$emit("$stateChangeSuccess");
              }else{
              };
              return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
            },function(err){
            console.log(err);
          }); 
     }
})