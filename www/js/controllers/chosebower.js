angular.module('controllers')
.controller('ChosebowerCtrl', function($scope,$state,$ionicHistory,$r,validation,toaster,$ionicModal) {
            $scope.form={};
            $scope.form.borrowlist;
            $scope.form.borrower_mobile;
            $scope.form.islimit='00';
            $scope.form.submits=true;
            $scope.form.thebower={};
            $scope.form.pageIndex=1;
            var msg;
            var load = function() {
                var reqBody= [{
                  "pageNum":1,
                  "numPerPage":5,
                }];
                $r("borrowerList", reqBody ).then(function(data){
                  console.log(data);
                   if(data.respHead.code=="0000"){
                        $scope.form.borrowlist=data.respBean.data;
                        $scope.form.pageIndex=data.respBean.page.pageIndex+1;
                        $scope.form.totalRecord=data.respBean.page.totalRecord;
                        $scope.form.surplusBorrwoer=data.respBean.surplusBorrwoer;
                    }else{
                      var msg=data.respHead.message;   
                      return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                    }
                },function(err){
                  console.log(err);
              });
          }
          $ionicModal.fromTemplateUrl('templates/showbower.html', {
            scope: $scope
          }).then(function(modal) {
            $scope.modal = modal;
          });
          $ionicModal.fromTemplateUrl('templates/searchbower.html', {
            scope: $scope
          }).then(function(modal) {
            $scope.search = modal;
          });
          $scope.closemodel = function() {
            $scope.modal.hide();
          };
          $scope.closesearch=function(){
            $scope.search.hide();
          }
          $scope.$on('$stateChangeSuccess', function() {
             console.log("$stateChangeSuccess");
             load();
           });
           
          $scope.limite=function(){
            if($scope.form.islimit=='01'){
               $scope.form.islimit='00';
             }else{
              $scope.form.islimit='01';
             }
          }
          
          $scope.addbower=function(){
            var checkstatus=false;
            console.log($scope.form);
            if(!checkstatus){
              if(!validation.isnull($scope.form.thebower.name)){
                  msg = "借款人姓名不能为空";
                  checkstatus=true;
              }  
            }
            if(!checkstatus){
              if(!validation.isnull($scope.form.thebower.identityCard)){
                    msg = "借款人身份证不能为空";
                    checkstatus=true;
              }
            }
            if(!checkstatus){
              if(!validation.isnull($scope.form.thebower.borrowerMobile)){
                    msg = "借款人手机号不能为空";
                    checkstatus=true;
              }else{
                    if(!validation.phone($scope.form.thebower.borrowerMobile)){
                       msg = "请填写正确的手机号";
                    checkstatus=true;
                    }
              }
            }
            if(!checkstatus){
              if(!validation.isnull($scope.form.thebower.BorrowAmount)){
                    msg = "借款人额度不能为空";
                    checkstatus=true;
              }else{
                 if(!validation.money($scope.form.thebower.BorrowAmount)){
                    msg = "借款人提款金额不合法";
                    checkstatus=true;
                 }else{
                   if($scope.form.thebower.BorrowAmount<=10){
                    msg = "借款人提款金额需大于10￥";
                    checkstatus=true;
                   }
                 }
                                  
              }
            }
               
            if(checkstatus==true){
              return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
            }else{
                var reqBody= [{
                        "borrowerMobile":$scope.form.thebower.borrowerMobile,
                        "riskControl":$scope.form.islimit,
                        "opType":"1",
                        "maxBorrowAmount":parseFloat($scope.form.thebower.BorrowAmount)*100,
                        "identityCard":$scope.form.thebower.identityCard,
                        "name":$scope.form.thebower.name
                      }];
                      $r("borrowerOp", reqBody ).then(function(data){
                        console.log(data);
                        if(data.respHead.code=="0000")
                        {
                            msg = "添加借款人成功"; 
                            $scope.modal.hide();
                            $scope.$emit("$stateChangeSuccess");
                            $scope.noMoreItemsAvailable = false;
                            return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
                            
                        }else{
                            msg=data.respHead.message;   
                            return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                        }
                      },function(err){
                        console.log(err);
                    });
            }
          }
          
          $scope.del_bower=function(item){
            console.log(item);
              var reqBody= [{
                        "borrowerMobile":item.borrowerMobile,
                        "riskControl":item.riskControl,
                        "borrowUserId":item.borrowUserId,
                        "opType":"2"
                      }];
                      $r("borrowerOp", reqBody ).then(function(data){
                        console.log(data);
                        if(data.respHead.code=="0000")
                        {
                            msg = "删除借款人成功";
                             $scope.$emit("$stateChangeSuccess");
                             $scope.search.hide();
                             $scope.noMoreItemsAvailable = false;
                            return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
                        }else{
                            var msg=data.respHead.message;   
                            return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                        }
                      },function(err){
                        console.log(err);
                    });
            
          }
          
          $scope.compalate=function(){
            console.log('conpalate');
             $ionicHistory.clearHistory();
                                $state.go("app.products",{}, {reload: true});
                                $ionicHistory.nextViewOptions({
                                   disableAnimate: true,
                                   disableBack: true
                                });
          }
          
          $scope.dosearch=function(){
            console.log('dosearch');
            if(!validation.phone($scope.form.borrowerMobile)){
                   msg = "请填写正确的手机号";
                   return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
            }
            $scope.search.show();
             var reqBody= [{
                  "pageNum":1,
                  "borrowerMobile":$scope.form.borrowerMobile
                }];
                $r("borrowerList", reqBody ).then(function(data){
                  console.log(data);
                   if(data.respHead.code=="0000"){
                        $scope.form.borrowlists=data.respBean.data;
                    }else{
                      var msg=data.respHead.message;   
                      return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                    }
                },function(err){
                  console.log(err);
              });
          }
          
          $scope.showaddmodel=function(){
            $scope.form.tirtle="增加借款人";
            $scope.form.modelsttus="add";
            $scope.form.thebower={};
            $scope.modal.show();
          }
          
          $scope.update_bower=function(item){
            $scope.form.tirtle="修改借款人";
            $scope.form.modelsttus="update";
            $scope.form.thebower=item;
            $scope.form.thebower.BorrowAmount=parseFloat($scope.form.thebower.maxBorrowAmount)/100;
            $scope.search.hide();
            $scope.modal.show();
            console.log($scope.form.thebower);
          }
          $scope.doupdate=function(item){
            console.log(item);
            console.log('doupdate');
             var checkstatus=false;
            console.log($scope.form);
            if(!checkstatus){
              if(!validation.isnull($scope.form.thebower.BorrowAmount)){
                    msg = "借款人额度不能为空";
                    checkstatus=true;
              }else{
                 if(!validation.money($scope.form.thebower.BorrowAmount)){
                    msg = "借款人提款金额不合法";
                    checkstatus=true;
                 }else{
                   if($scope.form.thebower.BorrowAmount<=10){
                    msg = "借款人提款金额需大于10￥";
                    checkstatus=true;
                   }
                 }
                                  
              }
            }
               
            if(checkstatus==true){
              return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
            }else{
                var reqBody= [{
                        "borrowUserId":item.borrowUserId,    
                        "opType":"3",
                        "maxBorrowAmount":parseFloat($scope.form.thebower.BorrowAmount)*100,
                        'borrowerMobile':item.borrowerMobile,
                        'riskControl':item.riskControl
                      }];
                      $r("borrowerOp", reqBody ).then(function(data){
                        console.log(data);
                        if(data.respHead.code=="0000")
                        {
                            msg = "修改借款人成功"; 
                            $scope.modal.hide();
                            $scope.$emit("$stateChangeSuccess");
                            $scope.noMoreItemsAvailable = false;
                            return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
                            
                        }else{
                            msg=data.respHead.message;   
                            return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                        }
                      },function(err){
                        console.log(err);
                    });
            }
          }
          
          $scope.noMoreItemsAvailable = false;
  
          $scope.loadMoreData = function() {
             var reqBody= [{
                  "pageNum":$scope.form.pageIndex,
                  "numPerPage":5,
                }];
             $r("borrowerList", reqBody).then(function(data){
                console.log(data);
                 if(data.respHead.code=="0000"){
                      if($scope.form.borrowlist && $scope.form.borrowlist.length >= data.respBean.page.totalRecord){
                          $scope.noMoreItemsAvailable = true;
                      }else{
                        _.each(data.respBean.data, function(item){
                        $scope.form.borrowlist.push(item);
                        })
                        $scope.form.pageIndex=data.respBean.page.pageIndex+1;
                      }
                  }else{
                    $scope.noMoreItemsAvailable = true;
                    var msg=data.respHead.message;   
                    return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                  }
              },function(err){
                $scope.noMoreItemsAvailable = true;
                console.log(err);
            });
            $scope.$broadcast('scroll.infiniteScrollComplete');
            
          };
})
