angular.module('controllers', [])
.controller('AppCtrl', function($rootScope,$scope, $ionicModal, $timeout, validation, toaster, $state ,$rs, $cache , $ionicHistory,$ionicLoading) {
  $scope.form={};
  $scope.form.username=$cache.getUserName();
  $scope.form.personimg=$cache.getPersonimg();
  $scope.form.num=0;
  $scope.$on("token:invalidation", function(){
    $ionicHistory.clearHistory();
    $ionicHistory.nextViewOptions({
               disableAnimate: true,
               historyRoot: true
    });
    var msg="登录信息过期";
    toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
    $state.go("app.login");
  });
  $rootScope.$on("token:refresh", function(){
       $scope.form.username=$cache.getUserName();
       $scope.form.personimg=$cache.getPersonimg();
  });
  
  $scope.domenumove=function(){
    $scope.form.num+=1;
    if($scope.form.num%2==0){
      $scope.form.moveas="";
    }else{
      $scope.form.moveas="moveas";  
    }
    
  };

})

.controller('LoginCtrl', function($rootScope,$scope,$ionicModal, $timeout, validation, toaster, $state ,$rs, $cache , $ionicHistory,$ionicLoading) {
    $scope.form={};
    $scope.form.usernames=$cache.getUserName();
    console.log($scope.form.usernames);
    $scope.form.submits=true;
    $scope.form.subtext="登录";
    var msg;
    // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
           console.log(validation.phone($scope.form.phone));
          if(validation.isnull($scope.form.phone)){
            if(validation.phone($scope.form.phone)){
                   if(validation.isnull($scope.form.password)){
                    $scope.form.submits=false;
                    $scope.form.subtext="登录中...";
                      var loginform = {
                        'phone':$scope.form.phone,
                        'password':$scope.form.password
                      };
                      $cache.setPhone($scope.form.phone);
                      $rs("login", loginform).then(function(data){
                        console.log(data);
                         if(data.code=="200"){
                           var user={
                                    'token':'admins',
                                    'username':data.docs[0].username,
                                    'idcard':data.docs[0]._id,
                                    'phone': data.docs[0].phone
                                  };
                                  $cache.setUser(user);
                                  $cache.setUserName(data.docs[0].username);
                                  $cache.setIdCard(data.docs[0]._id);
                                  if(data.docs[0].personfile){
                                    $cache.setPersonimg(data.docs[0].personfile.personimg);
                                  }
                                  
                                  $scope.form.username=$cache.getUserName();
                                  console.log($scope.form.username);
                                  $scope.form.submits=true;
                                  $scope.form.subtext="登录";
                                  $rootScope.$broadcast("token:refresh", data);
                                  $state.go("app.tabs");
                                 
                                  $ionicHistory.nextViewOptions({
                                     disableAnimate: true,
//                                     disableBack: true
                                     historyRoot: true
                                  });

                            }else{
                              $scope.form.submits=true;
                              $scope.form.subtext="登录";
                              var msg=data.message;
                              return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                            }
                        // $scope.closeLogin();
                        },function(err){
                          $scope.form.submits=true;
                          $scope.form.subtext="登录";
                          console.log(err);
                          var msg=err.message;
                          return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
                      }); 
                      
                    }else{
                        msg = "密码不能为空";
                        return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
                    }
              }else{
                        msg = "请填写正确的手机号";
                        return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
              }
          }else{
                        msg = "手机号不能为空";
                        return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
          }
 
    };
})

.controller('RegisterCtrl', function($scope,$ionicModal, $timeout, validation, toaster, $state ,$rs, $cache , $ionicHistory,$ionicLoading) {
    $scope.form={};
    $scope.form.submits=true;
    $scope.form.subtext="注册";
    $scope.doregister=function(){
        var msg;
        var checkstatus=false;
            console.log($scope.form);
            if(!checkstatus){
              if(!validation.isnull($scope.form.username)){
                  msg = "用户名不能为空";
                  checkstatus=true;
              }  
            }
            if(!checkstatus){
              if(!validation.isnull($scope.form.phone)){
                    msg = "用户名手机号不能为空";
                    checkstatus=true;
              }else{
                    if(!validation.phone($scope.form.phone)){
                       msg = "请填写正确的手机号";
                    checkstatus=true;
                    }
              }
            }
            if(!checkstatus){
              if(!validation.isnull($scope.form.password)){
                    msg = "密码不能为空";
                    checkstatus=true;
              }
            }
               
            if(checkstatus==true){
              return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
            }else{
                  $scope.form.submits=false;
                  $scope.form.subtext="注册中...";
                var reqBody= {
                        "username":$scope.form.username,
                        "phone":$scope.form.phone,
                        "password":$scope.form.password,
                        "personfile":{},
                      };
                      $rs("register", reqBody ).then(function(data){
                        console.log(data);
                        if(data.code=="200")
                        {
                            msg = "注册成功"; 
                            $scope.form.submits=true;
                            $scope.form.subtext="注册";
                            $state.go("app.login",{}, {reload: true});
                            return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
                            
                        }else{
                            $scope.form.submits=true;
                            $scope.form.subtext="注册";
                            msg=data.message;   
                            return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                        }
                      },function(err){
                        console.log(err);
                    });
            }
    };
    
})

.controller('PlaylistsCtrl', function($scope,$r,toaster) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
    $scope.form={};
  var reqBody= [{}];
     $r("borrowerList", reqBody ).then(function(data){
        console.log(data);
         if(data.respHead.code=="0000"){
              $scope.form.borrowlist=data.respBean.data;
          }else{
            var msg=data.respHead.message;   
            return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
          }
      },function(err){
        console.log(err);
    });

  $scope.noMoreItemsAvailable = false;
  
  $scope.loadMoreData = function() {
     var reqBody= [{}];
     $r("borrowerList", reqBody).then(function(data){
        console.log(data);
         if(data.respHead.code=="0000"){
              if(data.respBean.data && data.respBean.data.length > 50){
                  $scope.noMoreItemsAvailable = true;
              }
              _.each(data.respBean.data, function(item){
                $scope.form.borrowlist.push(item);
              })
              
          }else{
            var msg=data.respHead.message;   
            return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
          }
      },function(err){
        $scope.noMoreItemsAvailable = true;
        console.log(err);
    });
    $scope.$broadcast('scroll.infiniteScrollComplete');
    
  };


  
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
 
      var loginBody=   [ {
            "loginAccount": "admin",
            "password": "DFAF016D15CB38C487B5DB9E0ADA1FCC8A8959E0"
           } ];
            $r("login", loginBody ).then(function(data){
              console.log(data);
            },function(err){
              console.log(err);
          }); 

  $scope.getEnvTest = function() {
    // alert("hello wj");
    jfservice.getEnv("",
                        function(data){
                            alert(JSON.stringify(data));
                        }, function(err) {
                            alert(JSON.stringify(err));
                        });
            };
  $scope.RSAEncodeTest = function() {
  jfservice.RSAEncode("pwd123456",
                      function(data){
                          alert(JSON.stringify(data));
                      }, function(err) {
                          alert(JSON.stringify(err));
                      });
          };

  $scope.MD5EncodeTest = function() {
    jfservice.MD5Encode("teststr12345678901234567890123456789012345678901234567890",
                        function(data){
                            alert(JSON.stringify(data));
                        }, function(err) {
                            alert(JSON.stringify(err));
                        });
            };
  
})

.controller('SevenClickCtrl', function($scope, $element,$cache) {
  var count = 0;
  $scope.clicksever=function(){
        if (count < 7) {
      count++;
    } else {
        alert($scope.username+'/');
      count = 0;
    }
  };

})
.controller('SettingCtrl', function($scope) {

})
;
