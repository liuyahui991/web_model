angular.module('controllers')
.controller('orderDetailCtrl',function($scope,$r,$stateParams,$cache,$ionicModal,validation,toaster,$ionicHistory, $ionicPopup ,$state){

 $ionicModal.fromTemplateUrl('templates/sendmsg.html', {
        scope: $scope
      }).then(function(modal) {
        $scope.modal = modal;
    });

//rules
 $ionicModal.fromTemplateUrl('templates/showrules.html', {
        scope: $scope
      }).then(function(modal) {
        $scope.showrules = modal;
    });
    //orderDetail
$scope.form={};
$scope.form.countdown="发送验证码";
$scope.form.sendmsgs=true;
$scope.form.submits=true;
 $scope.form.clickstatus='tab1';
var order_list=[{
        "orderId":$stateParams.orderId,
   }];
 $r("orderDetail", order_list ).then(function(data){
        $scope.form.order_etail=data.respBean.data[0];
  },function(err){
        console.log(err);
 }); 
//bank
if($cache.getBank('withdraw')){
      $scope.form.bank=$cache.getBank('withdraw');
}else{
      var product_bank=[{}];
      $r("bankCardList", product_bank ).then(function(data){
            $scope.form.bank=data.respBean.data[0];
       },function(err){
             console.log(err);
      }); 
}
    //sendmsg
      $scope.payorder=function(){
         $scope.modal.show();
      }
      $scope.closemodal = function() {
          $scope.modal.hide();
        };
      //rules
      $scope.showrulesview=function(){
         $scope.showrules.show();
      }
      $scope.closerules = function() {
          $scope.showrules.hide();
        };
      $scope.tabshow=function(status){
          $scope.form.clickstatus=status;
      }

      $scope.sendmsg=function(){

    if($scope.form.sendmsgs==true){
        $scope.form.sendmsgs=false;
        var timer=40;
        var myTime = setInterval(function() 
          { 
            timer--;
            $scope.form.countdown='等待'+timer+ 's';
            $scope.$digest(); // 通知视图模型的变化
          }, 1000); // 倒计时10-0秒，但算上0的话就是11s ]

         setTimeout(function() { 
          clearInterval(myTime);
           $scope.form.sendmsgs=true;
           $scope.form.countdown="重新发送"; 
           $scope.$digest(); 
        }, 41000);

          var sendmsg=[{
              "bizType":"1000",//1000 pay; 1001 out
          }];
        $r("getValidateNo", sendmsg).then(function(data){
            console.log(data);
         },function(err){
            console.log(err);
        }); 

      }else{
                var msg="验证码已发送";   
                return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
      }
    }
      $scope.checkmsg=function(obg){
        console.log($scope.form.submits);
  
        console.log(obg);
         var msg;
          if(validation.isnull(obg.msg))
          {
              if(validation.isnull(obg.password))
              {
                  $scope.form.submits=false;
                //doing something orderPay
                var order_pay=[{
                  "orderId":obg.order_etail.orderId,
                  "password":obg.password,
                  "checkCode":obg.msg,
                  "bizType":"1000",
                  "amountTotal":obg.order_etail.amountTotal,
                  "cardNo":obg.bank.cardNo,
                  "payType":"01",
                  "productCode":obg.order_etail.productCode,
                  "amountFee":obg.order_etail.amountFee,
                  "amountProduct":obg.order_etail.amountProduct

                }];
                $r("orderPay", order_pay).then(function(data){
                        console.log(data);
                        var msg;
                        msg=data.respHead.message;
                        if(data.respHead.code==="0000"){
                              if (obg.order_etail.productCode==="C") {
                                  $ionicPopup.alert({
                                    title:"购买成功",
                                    template:"按下一步增加借款人",
                                    buttons:[{
                                      text:"下一步",
                                      onTap:function(){
                                        $scope.modal.hide();
                                        $state.go("app.chosebowerbuy");
                                      }
                                    }]
                                  });
                              }else{
                                  $scope.form.submits=true;
                                  $scope.modal.hide();
                                  $ionicHistory.goBack();
                                  msg="购买成功";
                                  return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
                              };
                        }else{
                          $scope.form.submits=true;
                          return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
                        };
                    console.log(data);
                 },function(err){
                      $scope.form.submits=true;
                      console.log(err);
                      var msg=err.respHead.message;   
                            return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                }); 

              }else{
                 msg = "登录密码为空";
                 return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
              }
          }else{
            msg = "验证码为空";
            return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
          }
      }

})