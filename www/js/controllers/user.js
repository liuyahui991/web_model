angular.module('controllers')
.controller('PersonCtrl',function($scope,$r, $q,$rs,$cache,$rootScope){
  $scope.form.username=$cache.getUserName();
   $scope.form.personimg=$cache.getPersonimg();
   console.log($scope.form);
    $rootScope.$on("token:refresh", function(){
       $scope.form.username=$cache.getUserName();
       $scope.form.personimg=$cache.getPersonimg();
  });
})
.controller('PersonfileCtrl',function($scope,$r, $q,$rs,validation,$cache,toaster,$rootScope){
// productslist
  $scope.form={};
  var load=function() {
       var finduser ={
         "id":$cache.getIdCard()
       };
        $rs("finduser", finduser ).then(function(data){
          $scope.products=data.docs[0];
          $scope.form=data.docs[0].personfile;
          if(data.docs[0].personfile){
            $scope.personimg=data.docs[0].personfile.personimg;
          }
//          $scope.form.personimage=data.docs[0].personfile.personimg;
//          $scope.form.username=data.docs[0].personfile.username;
//          $scope.form.flag=data.docs[0].personfile.flag;
//          $scope.form.content=data.docs[0].personfile.content;
          console.log(data);
          }, function(err){
          console.log(err);
        });
  };
  $scope.$on('$ionicView.enter', function() {
             console.log("$ionicView.enter");
        load();
  });
  $scope.success = function($message, $file) {
          // alert("imageURI");
          // 需要在最后保存的时候保存gallery 下面的media api
          var data;
          if (typeof $message == "string") {
            data = angular.fromJson($message);
          } else {
            data = $message;
          }
            console.log($file);
             console.log(data);
             $scope.form.personimg=data.product;
             if(data.product){
                                    $cache.setPersonimg(data.product);
             }
          $rootScope.$broadcast("token:refresh", data);
  };
  $scope.uppersonfile=function(obj){
           console.log(obj);
            var msg;
        if(validation.isnull(obj.username)){
          if(validation.isnull(obj.content)){
            msg=null;
             var reqBody= {
                  "id":$cache.getIdCard(),
                  "username":obj.username,
                  "flag":obj.flag,
                  "content":obj.content,
                  "personimg":obj.personimg
                };
            $rs("personfile", reqBody).then(function(data){
              console.log(data);
              if(data.code=='200'){
                msg = "修改用户信息成功";
                $cache.setUserName(obj.username);
                 $rootScope.$broadcast("token:refresh", data);
                return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
              }else{
                msg = "修改用户信息失败";
                return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
              }
            },function(err){
              console.log(err);
            });
          }else{
              msg = "个人简介不能为空";
          }
        }else{
              msg = "用户名不能为空";
        }
        if(msg){
           return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
        }
   };
 
})
;