angular.module('starter')
.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })
//tabs
   .state('app.tabs', {
      url: "/tab",
      // abstract: true,
      views: {
      'menuContent': {
       templateUrl: "templates/tab.html",
        controller: 'ProductsCtrl'
      }
    }
    })
    .state('app.tabs.home', {
      url: "/home",
      views: {
        'home-tab': {
          templateUrl: "templates/products.html",
          controller: 'ProductsCtrl'
        }
      }
    })
    .state('app.tabs.productlist', {
    url: "/productlist",
    views: {
      'productlist-tab': {
        templateUrl: "templates/productlist.html",
        controller: 'ProductlistCtrl'
      }
    }
  })
//article
  .state('app.tabs.articlelist', {
    url: "/articlelist",
    views: {
      'article-tab': {
        templateUrl: "templates/articlelist.html",
        controller: 'ArticlelistCtrl'
      }
    }
  })
   //person
    .state('app.tabs.search', {
    url: "/search",
    views: {
      'search-tab': {
        templateUrl: "templates/search.html",
        controller: 'PersonCtrl'
      }
    }
  })
  
.state('app.login', {
    url: "/login",
    views: {
      'menuContent': {
        templateUrl: "templates/login.html",
        controller: 'LoginCtrl'
      }
    }
  })
  
.state('app.register', {
    url: "/register",
    views: {
      'menuContent': {
        templateUrl: "templates/register.html",
        controller: 'RegisterCtrl'
      }
    }
  })
  //chrite
.state('app.chrite', {
    url: "/chrite",
    views: {
      'menuContent': {
        templateUrl: "templates/chrite.html",
        controller: 'ChriteCtrl'
      }
    }
  })
    .state('app.playlists', {
      url: "/playlists",
      views: {
        'menuContent': {
          templateUrl: "templates/playlists.html",
          controller: 'PlaylistsCtrl'
        }
      }
    })

  .state('app.single', {
    url: "/playlists/:playlistId",
    views: {
      'menuContent': {
        templateUrl: "templates/playlist.html",
        controller: 'PlaylistCtrl'
      }
    }
  })


  // .state('app.products', {
  //   url: "/products",
  //   views: {
  //     'menuContent': {
  //       templateUrl: "templates/products.html",
  //       controller: 'ProductsCtrl'
  //     }
  //   }
  // })
  .state('app.products', {
    url: "/products",
    views: {
      'menuContent': {
        templateUrl: "templates/products.html",
        controller: 'ProductsCtrl'
      }
    }
  })
   
  .state('app.productsDetail', {
    url: "/productsDetail/:productCode",
    views: {
      'menuContent': {
        templateUrl: "templates/productsdetail.html",
        controller: 'ProductsDetailCtrl'
      }
    }
  })
  .state('app.makeBank', {
    url: "/makeBank",
    views: {
      'menuContent': {
        templateUrl: "templates/makebank.html",
        controller: 'makeBankCtrl'
      }
    }
  })
  .state('app.choseBank', {
    url: "/choseBank",
    views: {
      'menuContent': {
        templateUrl: "templates/chosebank.html",
        controller: 'choseBankCtrl'
      }
    }
  })
  .state('app.orderDetail', {
    url: "/orderDetail/:orderId",
    views: {
      'menuContent': {
        templateUrl: "templates/orderdetail.html",
        controller: 'orderDetailCtrl'
      }
    }
  })
//account
  .state('app.account', {
    url: "/account",
    views: {
      'menuContent': {
        templateUrl: "templates/account.html",
        controller: 'AccountCtrl'
      }
    }
  })

//withdrawal
  .state('app.withdrawal', {
    url: "/withdrawal",
    views: {
      'menuContent': {
        templateUrl: "templates/withdrawal.html",
        controller: 'WithdrawalCtrl'
      }
    }
  })

//earning
   .state('app.earnings', {
    url: "/earnings",
    views: {
      'menuContent': {
        templateUrl: "templates/earnings.html",
        controller: 'EarningsCtrl'
      }
    }
  })

//hadbought
   .state('app.hadbought', {
    url: "/hadbought",
    views: {
      'menuContent': {
        templateUrl: "templates/hadbought.html",
        controller: 'HadboughtCtrl'
      }
    }
  })

//tradingrecord
    .state('app.tradingrecord', {
    url: "/tradingrecord",
    views: {
      'menuContent': {
        templateUrl: "templates/tradingrecord.html",
        controller: 'TradingrecordCtrl'
      }
    }
  })

//sendmsg
     .state('app.sendmsg', {
    url: "/sendmsg",
    views: {
      'menuContent': {
        templateUrl: "templates/sendmsg.html",
        controller: 'SendmsgCtrl'
      }
    }
  })

////chosebower
//   .state('app.chosebower', {
//    url: "/chosebower",
//    views: {
//      'menuContent': {
//        templateUrl: "templates/chosebower.html",
//        controller: 'ChosebowerCtrl'
//      }
//    }
//  })

//chosebower
   .state('app.chosebower', {
    url: "/chosebower",
    views: {
      'menuContent': {
        templateUrl: "templates/chosebower_find.html",
        controller: 'ChosebowerCtrl'
      }
    }
  })
   //chosebower
   .state('app.chosebowerbuy', {
    url: "/chosebowerbuy",
    views: {
      'menuContent': {
        templateUrl: "templates/chosebower_buy.html",
        controller: 'ChosebowerCtrl'
      }
    }
  })
//transDetail
   .state('app.transdetail', {
    url: "/transdetail/:transFlowId",
    views: {
      'menuContent': {
        templateUrl: "templates/transdetail.html",
        controller: 'transdetailCtrl'
      }
    }
  })
//setting
  .state('app.setting', {
    url: "/setting",
    views: {
      'menuContent': {
        templateUrl: "templates/setting.html",
        controller: 'SettingCtrl'
      }
    }
  })

  //products
  .state('app.addproduct', {
    url: "/addproduct",
    views: {
      'menuContent': {
        templateUrl: "templates/addproduct.html",
        controller: 'AddproductCtrl'
      }
    }
  })

  //personfile
  .state('app.personfile', {
    url: "/personfile",
    views: {
      'menuContent': {
        templateUrl: "templates/personfile.html",
        controller: 'PersonfileCtrl'
      }
    }
  })
  ;
   $urlRouterProvider.otherwise('/app/login');
});
