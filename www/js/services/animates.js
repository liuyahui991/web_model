angular.module('AppAnimations', [])
  .animation('.fold-animation', ['$animateCss', function($animateCss) {
      return {
        enter: function(element, doneFn) {
          var height = element[0].offsetHeight;
          return $animateCss(element, {
            from: { left:'-100%' },
            to: { left:'0%' },
            duration: 1 // one second
          });
        }
      }
    }]);
  ;